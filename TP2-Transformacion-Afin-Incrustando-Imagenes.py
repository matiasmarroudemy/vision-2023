import cv2 as cv
import numpy as np

#Declaro las constantes de colores en rgb
rojo = (255,0,0)
verde = (0,255,0)
azul = (0,0,255)

#Cuento cuantos puntos se han dibujado en la imagen, inicia con el valor 3
puntosDibujados = 3

def elegirPuntos( event, x, y, flags, param):
    """
    @brief Funcion que se llama cuando ocurra un evento en el mouse.\n 
    Esta es encarga de realizar la transformacion con los tres puntos seleccionados y la imagen a pegar\n
    @param event - Evento que se dispara del m
    @param x - cordenada x en el punto posicionado del mouse
    @param y - cordenada y en el punto posicionado del mouse
    @param flags - bandera del estado del mouse (no lo usamos)
    @param param - Parametro opcional (no lo usamos)
    """
    global puntosDibujados, pt1, pt2, pt3
    if event == cv.EVENT_LBUTTONDOWN:
        if puntosDibujados == 3:
            print("punto 1")
            pt1 = (x,y)
            cv.circle(image, (x,y),3,rojo,-1)
            puntosDibujados = 2
            
        elif puntosDibujados == 2:
            print("punto 2")
            pt2 = (x,y)
            cv.circle(image, (x,y),3,verde,-1)
            puntosDibujados = 1

        elif puntosDibujados == 1:
            print("punto 3")
            pt3 = (x,y)
            cv.circle(image, (x,y),3,azul,-1)
            puntosDibujados = 0


def transformAfin(imagen, vertImgOrg, vertPtsEleg):
    """
    @brief Funcion que se encarga de realizar la transformacion con los tres puntos seleccionados y la imagen a pegar\n
    @param imagen - Imagen a pegar
    @param vertImgOrg - vertices de la imagen original
    @param vertPtsEleg - vertices de los puntos seleccionadops de la imagen principal

    """
    filas, columnas, ch = imagen.shape

    matriz = cv.getAffineTransform(vertImgOrg, vertPtsEleg)
    imagenTransformada = cv.warpAffine(imagen, matriz, (columnas, filas))

    return imagenTransformada

#Cargamos imagen principal
image = cv.imread('espacio.jpg')
#Obtenemos el tamaño de la imagen
(alto, ancho) = image.shape[:2]

#Cargamos la imagen a colocar dentro de la principal
image2paste = cv.imread('astronauta.jpg')
#Obtenemos el tamaño de la imagen
(alto_x, ancho_x) = image2paste.shape[:2]

while True:
    #Mostramos la imagen principal
    cv.imshow('Principal',image )
    #Obtenemos la entrada del teclado en 1ms
    opt = cv.waitKey(1) & 0xFF

    #Opcion para seleccionar los tres puntos en la imagen
    if opt == ord('a'):
        cv.namedWindow('Principal')
        #Llamamos al metodo encargado de ejecutarse cuando exista un evento del mouse, llamando a la funcion elegirPuntos
        cv.setMouseCallback('Principal', elegirPuntos)

    #Opcion para guardar la imagen
    if opt == ord('s'):
        #se definen los vértices de la imagen original y los 3 puntos seleccionados
        verticesImagenOriginal = np.float32([[0, 0 ],[ancho_x, 0 ],[0, alto_x]])
        verticesPuntosElegidos = np.float32([[ pt1 ],[ pt2 ],[ pt3 ]])

        #Llamamos a la funcion transformAfin para que realice la trasnformacion para pegar la imagen
        retTransf = transformAfin(image2paste, verticesImagenOriginal, verticesPuntosElegidos)
        #Accedemos a las cordenadas individuales de cada punto seleccionado
        (x1,y1) = pt1 ; (x2,y2) = pt2 ; (x3,y3) = pt3

        #Matriz Mascara para sumar las imagenes
        mask = np.array([[x1,y1],[x3,y3],[x3+x2-x1,y3+y2-y1],[x2,y2]],np.int32)
        #Se rellena la región de la máscara en la imagen de fondo con color negro (0,0,0)
        cv.fillPoly(image , [mask], (0,0,0), cv.LINE_AA)
        #Se recorta la imagen transformada para que coincida con las dimensiones de la imagen de fondo
        retTransf = retTransf[0:alto, 0:ancho] 
        #Combinamos las iamgenes de fondo con la imagen trasnformada
        resultado = cv.add(image, retTransf)
        #Guardamos la imagen resultante y luego la mostramos
        cv.imwrite('output1.jpg',resultado)
        cv.imshow('img',resultado)

    #Salimos del progama
    if opt == ord('e'):
        break


cv.destroyAllWindows()
