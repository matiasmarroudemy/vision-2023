# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Usando como base el programa anterior, crear un programa que permita seleccionar una
# porción rectangular de una imagen con el ratón, luego:
# *con la letra “g” lo guardamos a disco en una nueva imagen y salimos
# *con la letra “r” restauramos la imagen original y volvemos a realizar la selección
# *con la “q” salimos.
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import cv2 as cv 
import numpy as np

drawing = False #true si el mouse es presionado
xy_puntoInicialMouse = (-1, -1)
xy_puntoFinalMouse = (-1,-1)

img = cv.imread('testImagen.jpg')
imgRespaldo = cv.imread('testImagen.jpg')


def dibuja_rectangulo(event, x, y, flags, param):
    global xy_puntoInicialMouse,xy_puntoFinalMouse, drawing

    if event == cv.EVENT_LBUTTONDOWN:
        drawing = True
        xy_puntoInicialMouse = (x, y) 
        # print("Posicion inicial: ",str(xy_puntoInicialMouse))
    elif event == cv.EVENT_MOUSEMOVE:
        if drawing:
            img[:] = imgRespaldo[:] 
            cv.rectangle(img,xy_puntoInicialMouse,(x,y),(0,255,255),1)
    elif event == cv.EVENT_LBUTTONUP:
        drawing = False
        xy_puntoFinalMouse = (x, y)
        # print("Posicion final: ",str(xy_puntoFinalMouse))

def saveImg(xy_puntoInicialMouse, xy_puntoFinalMouse, img):
    xInicial, yInicial = xy_puntoInicialMouse
    xFinal, yFinal = xy_puntoFinalMouse

    ancho = abs(xFinal-xInicial)
    alto = abs(yFinal - yInicial)

    if xFinal < xInicial:
        xInicial, xFinal = xFinal, xInicial

    if yFinal < yInicial:
        yInicial, yFinal = yFinal, yInicial

    imgToSave = np.zeros((alto,ancho,3), np.uint8)
    try:
        imgToSave = img[yInicial:yInicial+alto, xInicial:xInicial+ancho].copy()
        cv.imwrite('imgRecortada.jpg',imgToSave)
        print("[saveImg] - La imagen recortada se guardo correctamente")
    except Exception as e:
        print("[saveImg] - Error al guardar la imagen")
        print("[saveImg] - Exception: ",str(e))
        

cv.namedWindow('image')
cv.setMouseCallback('image',dibuja_rectangulo)

while True:
    cv.imshow('image',img)
    
    opt = cv.waitKey(1) & 0xFF
    
    if opt == ord('q'):
        print("[MAIN] - El programa cerro correctamente")
        break
    elif opt == ord('g'):
        saveImg(xy_puntoInicialMouse, xy_puntoFinalMouse, img)
        img = cv.imread("testImagen.jpg")
        # imagenRecortada=mp
    elif opt == ord('r'):
        img = cv.imread("testImagen.jpg")
        print("[MAIN] - Se restauran valores predeterminados")

cv.destroyAllWindows()